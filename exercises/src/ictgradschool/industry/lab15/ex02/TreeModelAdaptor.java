package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;


import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;


public class TreeModelAdaptor implements TreeModel {

    private NestingShape root;

    public TreeModelAdaptor(NestingShape root){
        this.root = root;
    }

    @Override
    public Object getRoot() {
        return root;
    }

    @Override
    public Object getChild(Object parent, int index) {

        if(parent instanceof NestingShape){
            NestingShape pArent = (NestingShape) parent;
            return pArent.shapeAt(index);
        }
        else{return null;}
    }

    @Override
    public int getChildCount(Object parent) {
        if(parent instanceof NestingShape){
            NestingShape pArent = (NestingShape) parent;
            return pArent.shapeCount();
        }
        return 0;
    }

    @Override
    public boolean isLeaf(Object node) {
        if(node instanceof NestingShape){
            return false;
        }
        return true;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int result = -1;
        if(parent instanceof NestingShape){
            NestingShape pArent = (NestingShape) parent;
            result = pArent.indexOf((Shape) child);
        }
        return result;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }
}
