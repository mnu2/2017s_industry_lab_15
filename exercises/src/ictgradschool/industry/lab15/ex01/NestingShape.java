package ictgradschool.industry.lab15.ex01;

import com.sun.jmx.snmp.agent.SnmpGenericMetaServer;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class NestingShape extends Shape {

    private List<Shape> shapes= new ArrayList<>();

    public NestingShape(){

    }

    public NestingShape(int x, int y){

        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY){

        super(x, y, deltaX, deltaY);

    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height){

        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void move(int width, int height) {
        for (Shape shape : shapes) {
            shape.move(fWidth, fHeight);//they have to bounce within their window
        }
        super.move(width, height);
    }

    @Override
    public void paint(Painter painter) {

        painter.drawRect(fX, fY, fWidth, fHeight);
        painter.translate(fX, fY);
        for(Shape shape: shapes){
            shape.paint(painter);
        }
        painter.translate(-fX, -fY);


    }

    public void add(Shape child) throws IllegalArgumentException {

        if(shapes.contains(child)){
            throw new IllegalArgumentException("Already contains this shape");
        }
        else if ((child.getY() + child.getHeight()) > (fY + fHeight) ){
            throw new IllegalArgumentException("Child is too big!");
        }
        else if ( (child.getX() + child.getWidth()) > (fX + fWidth)){
            throw new IllegalArgumentException("Child is too bigg!");
        }
        else if (!(child.parent == (null))){throw new IllegalArgumentException("This child has a different parent");}

        else{
        shapes.add(child);
        child.addParent(this);
    }
    }

    public void remove(Shape shape) {
        if(shapes.contains(shape)){
            shapes.remove(shape);
            shape.addParent(null);
        }
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException{
        if (index > shapes.size() - 1 || index < 0){
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        else{
            return shapes.get(index);
        }
    }

    public int shapeCount(){
        return shapes.size();
    }

    public int indexOf(Shape child){
        if (shapes.contains(child)){
            return shapes.indexOf(child);
        }
        else {return -1;}
    }

    public boolean contains(Shape child){
        return shapes.contains(child);
    }

}
